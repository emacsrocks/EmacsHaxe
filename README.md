My custom emacs for Haxe
========================

Initial config
--------------

Clone the repository using the following :
```bash
git clone git@gitlab.com:Babouchot/EmacsHaxe.git .emacs.d
```

or
```bash
git clone git@gitlab.com:Babouchot/EmacsHaxe.git .emacs.d.haxe
```
and symlink .emacs.d to it when needed if you have several emacs configurations living in parallel.


run :
```
M-x package-refresh-contents
M-x package-install-selected-packages
```

note : there might be some errors while runing the package installation liek "Lisp nesting exceeds 'max-lisp-eval-depth'. Just run the command several times until all packages are installed. Or force install packages manually.
(setq max-lisp-eval-depth 10000) might help.

close emacs and reopen to reload config.

